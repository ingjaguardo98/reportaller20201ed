var productosObtenidos;

function getProducts() {
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            //console.table(JSON.parse(request.responseText).value);
            productosObtenidos = request.responseText;
            procesarProductos();
        }
    };

    request.open("GET", url, true);
    request.send();
}

function procesarProductos() {
    var JSONProductos = JSON.parse(productosObtenidos);
    //alert(JSONProductos.value[0].ProductName);
    var divTabla = document.getElementById("divTablaProductos");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");

    for (var i = 1; i < JSONProductos.value.length; i++) {
        //console.log(JSONProductos.value[i].ProductName);
        var nuevaFila = document.createElement("tr");
        var columnNombre = document.createElement("td");
        columnNombre.innerText = JSONProductos.value[i].ProductName;
        var columnPrecio = document.createElement("td");
        columnPrecio.innerText = JSONProductos.value[i].UnitPrice;
        var columnStock = document.createElement("td");
        columnStock.innerText = JSONProductos.value[i].UnitsInStock;
        nuevaFila.appendChild(columnNombre);
        nuevaFila.appendChild(columnPrecio);
        nuevaFila.appendChild(columnStock);
        tbody.appendChild(nuevaFila);
    }

    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}