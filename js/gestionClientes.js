var clientesObtenidos;

function getCustomers() {
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            //console.table(JSON.parse(request.responseText).value);
            clientesObtenidos = request.responseText;
            procesarClientes();
        }
    };

    request.open("GET", url, true);
    request.send();
}

function procesarClientes() {
    var JSONClientes = JSON.parse(clientesObtenidos);
    //alert(JSONClientes.value[0].ProductName);
    var divTabla = document.getElementById("divTablaClientes");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");

    var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"

    for (var i = 1; i < JSONClientes.value.length; i++) {
        //console.log(JSONClientes.value[i].ContactName);
        var nuevaFila = document.createElement("tr");
        var columnName = document.createElement("td");
        columnName.innerText = JSONClientes.value[i].ContactName;
        var columnCity = document.createElement("td");
        columnCity.innerText = JSONClientes.value[i].City;
        var columnFlag = document.createElement("td");
        var imgFlag = document.createElement("img");
        if (JSONClientes.value[i].Country === 'UK') {
            imgFlag.src = rutaBandera + "United-Kingdom" + ".png";
        } else {
            imgFlag.src = rutaBandera + JSONClientes.value[i].Country + ".png";
        }
        imgFlag.style.width = "70px";
        imgFlag.style.height = "40px";
        columnFlag.appendChild(imgFlag);
        nuevaFila.appendChild(columnName);
        nuevaFila.appendChild(columnCity);
        nuevaFila.appendChild(columnFlag);
        tbody.appendChild(nuevaFila);
    }

    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}